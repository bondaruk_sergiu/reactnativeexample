const processChange = (change, state, action) => (
  typeof change === 'object' ? change : change(action, state)
);

export const mergeIn = change => (state, action) => ({
  ...state,
  ...(processChange(change, state, action)),
});

export const mergeInState = (stateProp, change) => (state, action) => ({
  ...state,
  [stateProp]: {
    ...state[stateProp],
    ...(processChange(change, state, action)),
  },
});

export const getSafeValue = (state, valueName, defaultValue = null) => {
  const valueNameParts = valueName.split('.');
  let tempValue = state;
  for (let i = 0; i < valueNameParts.length; i += 1) {
    if (tempValue[valueNameParts[i]]) {
      tempValue = tempValue[valueNameParts[i]];
    } else {
      return defaultValue;
    }
  }
  return tempValue;
};
